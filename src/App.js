import {userInfo} from "./info";

function App() {
  return (
    <div>
      <h5>{userInfo.lastname} {userInfo.firstname}</h5>
      <img src={userInfo.avatar} alt="" width={300}></img>
      <p>{userInfo.age}</p>
      <p>{userInfo.age <= 35 ? "Anh ấy còn trẻ" : "Anh ấy đã già"}</p>
      <ul>
        {userInfo.language.map((e, index) => {
          return <li key={index}>{e}</li>;
        })}
      </ul>
    </div>
  );
}

export default App;
