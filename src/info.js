import icon from "./assets/images/avatardefault_92824.png";

export const userInfo = {
    firstname: 'Hoang',
    lastname: 'Pham',
    avatar: icon,
    age: 30,
    language: ['Vietnamese', 'Japanese', 'English']
}